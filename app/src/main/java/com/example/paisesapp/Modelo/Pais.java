package com.example.paisesapp.Modelo;

import java.io.Serializable;

public class Pais implements Serializable {
    String nombre;
    String himno;
    String url;
    String videoUrl;
    String himnoUrl, detalles;

    public Pais(String nombre,String videoUrl ,String himno, String url,String himnoUrl, String detalles) {
        this.nombre = nombre;
        this.himno = himno;
        this.url = url;
        this.videoUrl = videoUrl;
        this.himnoUrl = himnoUrl;
        this.detalles=detalles;
    }


    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public String getHimno() {
        return himno;
    }

    public void setHimno(String himno) {
        this.himno = himno;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getHimnoUrl() {
        return himnoUrl;
    }

    public void setHimnoUrl(String himnoUrl) {
        this.himnoUrl = himnoUrl;
    }

    @Override
    public String toString() {
        return nombre;
    }
}

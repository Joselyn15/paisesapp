package com.example.paisesapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.paisesapp.DetalleActivity;
import com.example.paisesapp.Modelo.Pais;
import com.example.paisesapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    List<Pais> ListDatos;
    Context context;

    public Adapter(List<Pais> listDatos, Context context) {
        ListDatos = listDatos;
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_pais, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.dato1.setText(ListDatos.get(position).getNombre());
        Picasso.with(getContext()).load(ListDatos.get(position).getUrl()).into(holder.img);
    }

    @Override
    public int getItemCount() {
        return ListDatos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView dato1;
        private ImageView img;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            dato1 = (TextView) itemView.findViewById(R.id.textNombre);
            img = (ImageView) itemView.findViewById(R.id.ivMovieImage);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Pais movie = ListDatos.get(getAdapterPosition());
            Intent intent = new Intent(getContext(), DetalleActivity.class);
            intent.putExtra("PAIS", movie);
            getContext().startActivity(intent);

        }


    }
}
